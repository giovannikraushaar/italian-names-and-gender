# Italian Names and Gender

## Introduction

Data is provided in the form of a *.csv* file. The table has two columns: `name` and `gender`. Gender is encoded as follows:

* `'M'` for male (maschile)
* `'F'` for female (femminile)
* `'U'` for unisex

## Warning

To be used only in a context of italian names. In case of names in other languages consider to join this dataset with one or more additional ones and pay particular attention to the intersection as spurious results may araise. For example *Andrea* in italian is exclusevly a name for males, whereas in english is mostly a neme for females.

## Source

`src/` contains the source code for scrpaing the names from the web.

##### Tags
gender, checking, italian, names, nomi, italiani, data, genere.